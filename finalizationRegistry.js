const registry = new FinalizationRegistry(console.log);

function example() {
  const x = {};
  registry.register(x, 'x has been collected');
}

example();

while (true) {};
